export class Norwegian_Blue {
    constructor(voltage, isNailed) {
        this.isNailed = isNailed;
        this.voltage = voltage;
        this.baseSpeed = 12;
    }

    getSpeed() {
        return (this.isNailed) ? 0 : this.getBaseSpeedWithVoltage(this.voltage);
    }
    getBaseSpeedWithVoltage(voltage) {
        return Math.min(24, voltage * this.baseSpeed);
    }
}
