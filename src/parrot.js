import { European } from './European';
import { African } from './African';
import { Norwegian_Blue } from './Norwegian_Blue';
export class Parrot {
    constructor(parrotType) {
       this.parrotType = null;


            if (parrotType instanceof European){
                this.parrotType = parrotType;
            }
            if (parrotType instanceof African){
                this.parrotType = parrotType;
            }
            if (parrotType instanceof Norwegian_Blue){
                this.parrotType = parrotType;
            }

    }

    getSpeed() {
       return this.parrotType.getSpeed();
    }

}
