export class African {
    constructor(numberOfCoconuts) {

        this.baseSpeed = 12;
        this.loadFactor = 9;
        this.numberOfCoconuts = numberOfCoconuts;

    }

    getSpeed() {
        return Math.max(0, this.baseSpeed - this.loadFactor * this.numberOfCoconuts);
    }
}